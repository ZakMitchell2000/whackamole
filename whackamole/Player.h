#pragma once
// Library needed for using sprites, textures, and fonts
#include <SFML/Graphics.hpp>
// Library for handling collections of objects
#include <vector>
class Player
{
public: // access level (to be discussed later)
// Constructor
	Player(sf::Texture& playerTexture, sf::Vector2u screenSize);
	// Variables (data members) used by this class

	// Functions to call Player-specific code
	void Input();
	void Update(sf::Time frameTime);

	sf::Sprite sprite;
	sf::Vector2f velocity;
	float speed;
};




