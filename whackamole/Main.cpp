// Library Includes
#include <SFML/Graphics.hpp> 
#include <SFML/Audio.hpp>
#include <string>
#include "Player.h"
#include <cstdlib>

int main()
{
	// Declare our SFML window, called gameWindow
	sf::RenderWindow gameWindow;
	// Set up the SFML window, passing the dimmensions and the window name
	gameWindow.create(sf::VideoMode(750, 750), "Whack A Mole", sf::Style::Titlebar | sf::Style::Close);

	// -----------------------------------------------
	// Game Setup
	// -----------------------------------------------
	// Sprite Texture---------------------------------
	// Declare a texture variable called playerTexture
	sf::Texture playerTexture;
	// Load up our texture from a file path
	playerTexture.loadFromFile("Assets/Graphics/player.png");
	// Declare a player object
	Player playerObject(playerTexture, gameWindow.getSize());
	// -----------------------------------------------
	// Hole-------------------------------------------
	sf::Texture holeTexture;
	holeTexture.loadFromFile("Assets/Graphics/hole.png");

	sf::Sprite holeSprites[9];
	// Loop over each sprite, setting their textures
	for (int i = 0; i < 9; i++)
	{
		holeSprites[i].setTexture(holeTexture);
	}

	int xPos = 175;
	int yPos = 175;

	// Loop for setting positions
	for (int i = 0; i < 9; i++)
	{
		holeSprites[i].setPosition(
		xPos - holeTexture.getSize().x / 2,
		yPos - holeTexture.getSize().y / 2
		);
		xPos = xPos + 200;
		if (xPos > 575)
		{
			xPos = 175;
			yPos = yPos + 200;
		}
	}
	// -----------------------------------------------
	// Mole-------------------------------------------
	sf::Texture moleTexture;
	moleTexture.loadFromFile("Assets/Graphics/mole.png");

	sf::Sprite moleSprites[9];

	for (int i = 0; i < 9; i++)
	{
		moleSprites[i].setTexture(moleTexture);
	}

	srand(time(NULL));

	bool moleSpawn = false;

	int moleNum = rand() % 9;

	if (moleSpawn == false)
	{
		moleSprites[moleNum].setPosition(
			holeSprites[moleNum].getPosition().x,
			holeSprites[moleNum].getPosition().y
		);
		moleSpawn = true;
	}
	// -----------------------------------------------
	// Mole Despawn-----------------------------------
	// Create a time value to store the total time between each item spawn
	sf::Time itemSpawnDuration = sf::seconds(5.0f);
	// Create a timer to store the time remaining for our game
	sf::Time itemSpawnRemaining = itemSpawnDuration;
	// -----------------------------------------------
	// Music-------------------------------
	sf::Music gameMusic;
	gameMusic.openFromFile("Assets/Audio/music.ogg");
	gameMusic.play();

	sf::SoundBuffer pickupSoundBuffer;
	pickupSoundBuffer.loadFromFile("Assets/Audio/pickup.wav");
	sf::Sound pickupSound;
	pickupSound.setBuffer(pickupSoundBuffer);

	sf::SoundBuffer victorySoundBuffer;
	victorySoundBuffer.loadFromFile("Assets/Audio/victory.ogg");
	sf::Sound victorySound;
	victorySound.setBuffer(victorySoundBuffer);
	// -----------------------------------------------
	// Fonts------------------------------------------
	sf::Font gameFont;
	gameFont.loadFromFile("Assets/Fonts/mainFont.ttf");
	sf::Text titleText;
	titleText.setFont(gameFont);
	titleText.setString("Whack A Mole");
	titleText.setCharacterSize(24);
	titleText.setFillColor(sf::Color::Black);
	titleText.setStyle(sf::Text::Bold | sf::Text::Italic);
	titleText.setPosition(gameWindow.getSize().x / 2 - titleText.getLocalBounds().width / 2, 30);
	// -----------------------------------------------
	// Score------------------------------------------
	int score = 0;

	sf::Text scoreText;
	scoreText.setFont(gameFont);
	scoreText.setString("Score: " + std::to_string(score));
	scoreText.setCharacterSize(16);
	scoreText.setFillColor(sf::Color::Black);
	scoreText.setPosition(30, 30);
	// -----------------------------------------------
	// Timer------------------------------------------
	sf::Text timerText;
	timerText.setFont(gameFont);
	timerText.setString("Time Remaining: 0");
	timerText.setCharacterSize(16);
	timerText.setFillColor(sf::Color::Black);
	timerText.setPosition(gameWindow.getSize().x - timerText.getLocalBounds().width - 30, 30);

	sf::Time timeLimit = sf::seconds(60.0f);
	sf::Time timeRemaining = timeLimit;
	sf::Clock gameClock;

	// -----------------------------------------------
	// Sprite Movement--------------------------------
	sf::Vector2f playerVelocity(0.0f, 0.0f);

	float speed = 100.0f;

	// -----------------------------------------------
	// Game Over Text---------------------------------
	bool gameOver = false;

	sf::Text gameOverText;
	gameOverText.setFont(gameFont);
	gameOverText.setString("GAME OVER\n\nPress R to restart\nor Q to quit");
	gameOverText.setCharacterSize(24);
	gameOverText.setFillColor(sf::Color::Black);
	gameOverText.setStyle(sf::Text::Bold | sf::Text::Italic);
	gameOverText.setPosition(gameWindow.getSize().x / 2 - gameOverText.getLocalBounds().width / 2, 150);
	// -----------------------------------------------
	// Game Loop--------------------------------------
	while (gameWindow.isOpen())
	{
		// -----------------------------------------------
		// Input Section
		// -----------------------------------------------
		sf::Event gameEvent;
		while (gameWindow.pollEvent(gameEvent))
		{
			// This section will be repeated for each event waiting to be processed
			if (gameEvent.type == sf::Event::Closed)
			{
				gameWindow.close();
			}
			// -----------------------------------------------
			if (gameOver)
			{
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::R))
				{
					// Reset the game
					score = 0;
					timeRemaining = timeLimit;
					gameMusic.play();
					gameOver = false;
				}
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
				{
					// End the game
					gameWindow.close();
				}

			}
			// -----------------------------------------------
			// Movement---------------------------------------
			playerObject.Input();
			// -----------------------------------------------
		}
		// -----------------------------------------------
		// Update Section
		// -----------------------------------------------
		scoreText.setString("Score: " + std::to_string(score));
		// Timer -----------------------------------------
		sf::Time frameTime = gameClock.restart();
		timeRemaining = timeRemaining - frameTime;
		timerText.setString("Time Remaining: " + std::to_string((int)timeRemaining.asSeconds()));

		if (timeRemaining.asSeconds() <= 0)
		{
			// Don't let the time go lower than 0
			timeRemaining = sf::seconds(0);

			if (gameOver == false)
			{
				gameOver = true;

				gameMusic.stop();
				
				victorySound.play();
			}
		}
		// Movement---------------------------------------
		playerObject.Update(frameTime);
		// -----------------------------------------------
		// MoleDespawn------------------------------------
		if (!gameOver)
		{
			itemSpawnRemaining = itemSpawnRemaining - frameTime;
			// Check if time remaining to next spawn has reached 0
			if (itemSpawnRemaining <= sf::seconds(0.0f))
			{
				moleSpawn = false;
				itemSpawnRemaining = itemSpawnDuration;
			}

			if (moleSpawn == false)
			{
				moleNum = rand() % 9;
				moleSprites[moleNum].setPosition(
					holeSprites[moleNum].getPosition().x,
					holeSprites[moleNum].getPosition().y
				);

				moleSpawn = true;
			}

			sf::FloatRect itemBounds = moleSprites[moleNum].getGlobalBounds();
			sf::FloatRect playerBounds = playerObject.sprite.getGlobalBounds();
			if (itemBounds.intersects(playerBounds))
			{
				moleSpawn = false;
				itemSpawnRemaining = itemSpawnDuration;
				score = score + 100;
				pickupSound.play();
			}
		}
		// -----------------------------------------------
		// Draw Section
		// -----------------------------------------------
		// Clear the window to a single colour
		gameWindow.clear(sf::Color::Cyan);

		if (!gameOver)
		{
			// Draw sprite
			for (int i = 0; i < 9; i++)
			{
				gameWindow.draw(holeSprites[i]);
			}

			if (moleSpawn == true)
			{
				gameWindow.draw(moleSprites[moleNum]);
			}

			gameWindow.draw(playerObject.sprite);
		}
		// Draw title text
		gameWindow.draw(titleText);

		// Draw Score
		gameWindow.draw(scoreText);

		// Draw Timer
		gameWindow.draw(timerText);

		// Draw Gameover text
		if (gameOver)
		{
			gameWindow.draw(gameOverText);
		}

		// Display the window contents on the screen
		gameWindow.display();

	}
	// End of Game Loop
	return 0;
}
// End of main() Function